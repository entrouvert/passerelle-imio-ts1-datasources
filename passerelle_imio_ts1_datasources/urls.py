from django.urls import re_path
from .views import (
    DatasourcesView,
    MotivationtermAddView,
    MotivationtermDeleteView,
    MotivationtermUpdateView,
    DestinationtermAddView,
    DestinationtermDeleteView,
    DestinationtermUpdateView,
)


urlpatterns = [
    re_path(r'^(?P<slug>[\w,-]+)/data$', DatasourcesView.as_view(), name='DatasourcesView-data'),
]

management_urlpatterns = [
    re_path(
        r'^(?P<connector_slug>[\w,-]+)/motivationterm/add/',
        MotivationtermAddView.as_view(),
        name='motivationterm-add',
    ),
    re_path(
        r'^(?P<connector_slug>[\w,-]+)/motivationterm/(?P<pk>[\w,-]+)/delete/',
        MotivationtermDeleteView.as_view(),
        name='motivationterm-delete',
    ),
    re_path(
        r'^(?P<connector_slug>[\w,-]+)/motivationterm/(?P<pk>[\w,-]+)/update/',
        MotivationtermUpdateView.as_view(),
        name='motivationterm-update',
    ),
    re_path(
        r'^(?P<connector_slug>[\w,-]+)/destinationterm/add/',
        DestinationtermAddView.as_view(),
        name='destinationterm-add',
    ),
    re_path(
        r'^(?P<connector_slug>[\w,-]+)/destinationterm/(?P<pk>[\w,-]+)/delete/',
        DestinationtermDeleteView.as_view(),
        name='destinationterm-delete',
    ),
    re_path(
        r'^(?P<connector_slug>[\w,-]+)/destinationterm/(?P<pk>[\w,-]+)/update/',
        DestinationtermUpdateView.as_view(),
        name='destinationterm-update',
    ),
]
